const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const Book = require("./schemas/Book.model");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const dbLoc = "mongodb://localhost:27017/BooksDB";

mongoose.connection.on("connected", () => {
  console.log("Mongoose default conn open.");
});

mongoose.connect(dbLoc, { useNewUrlParser: true });

app.get("/", (req, res) => {
  res.send("Mongo db crud example!");
});
app.get("/books", (req, res) => {
  console.log("getting all books");
  Book.find({}).exec((err, books) => {
    if (err) {
      res.send("Error has occurred!");
    } else {
      console.log(books);
      res.json(books);
    }
  });
});

app.get("/books/:id", (req, res) => {
  console.log("get one book");
  const { id } = req.params;
  Book.findOne({
    _id: id
  }).exec((err, book) => {
    if (err) {
      res.send("Error occurred!");
    } else {
      console.log(book);
      res.json(book);
    }
  });
});

app.post("/books", (req, res) => {
  const newBook = new Book();
  const { title, author, category } = req.body;
  newBook.title = title;
  newBook.author = author;
  newBook.category = category;

  newBook.save((err, book) => {
    if (err) {
      res.send("Error saving Book");
    } else {
      console.log(book);
      res.send(book);
    }
  });
});

app.put("/books/:id", (req, res) => {
  Book.findOneAndUpdate(
    {
      _id: req.params.id
    },
    { $set: { title: req.body.title } },
    { upsert: true },
    (err, newBook) => {
      if (err) {
        res.send("Error occurred!");
      } else {
        console.log(newBook);
        res.send(newBook);
      }
    }
  );
});

app.delete("/books/:id", (req, res) => {
  Book.findOneAndRemove(
    {
      _id: req.params.id
    },
    err => {
      if (err) {
        res.send("Error occurred");
      } else {
        res.status(204);
      }
    }
  );
});

const port = 3001;

app.listen(port, () => {
  console.log("Server is listening on ", port);
});
